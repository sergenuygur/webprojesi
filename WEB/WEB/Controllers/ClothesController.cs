﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WEB.Models;

namespace WEB.Controllers
{
    public class ClothesController : Controller
    {
        private ElbiseStoreEntities db = new ElbiseStoreEntities();

        // GET: Clothes
        public ActionResult Index()
        {
            if (Session["user"] == null)
                return RedirectToAction("Login");

            return View(db.Clothes.ToList());
        }

        // GET: Clothes/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["user"] == null)
                return RedirectToAction("Login");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cloth cloth = db.Clothes.Find(id);
            if (cloth == null)
            {
                return HttpNotFound();
            }
            return View(cloth);
        }

        // GET: Clothes/Create
        public ActionResult Create()
        {
            if (Session["user"] == null)
                return RedirectToAction("Login");

            return View();
        }

        // POST: Clothes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ModelName,Color,Size,Price,ImageURL")] Cloth cloth)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["ModelImage"].ContentLength != 0)
                {
                    string pathSave = Server.MapPath("~/foto/");
                    string fileName = Path.GetFileName(cloth.ModelName + "." + Request.Files["ModelImage"].FileName.Split('.')[1]);
                    cloth.ImageURL = "/foto/" + fileName;
                    Request.Files["ModelImage"].SaveAs(Path.Combine(pathSave, fileName));
                }

                db.Clothes.Add(cloth);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cloth);
        }

        // GET: Clothes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["user"] == null)
                return RedirectToAction("Login");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cloth cloth = db.Clothes.Find(id);
            if (cloth == null)
            {
                return HttpNotFound();
            }
            return View(cloth);
        }

        // POST: Clothes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ModelName,Color,Size,Price,ImageURL")] Cloth cloth)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["ModelImage"].ContentLength != 0)
                {
                    string pathSave = Server.MapPath("~/foto/");
                    string fileName = Path.GetFileName(cloth.ModelName + "." + Request.Files["ModelImage"].FileName.Split('.')[1]);
                    cloth.ImageURL = "/foto/" + fileName;
                    Request.Files["ModelImage"].SaveAs(Path.Combine(pathSave, fileName));
                }

                db.Entry(cloth).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cloth);
        }

        // GET: Clothes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["user"] == null)
                return RedirectToAction("Login");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cloth cloth = db.Clothes.Find(id);
            if (cloth == null)
            {
                return HttpNotFound();
            }
            return View(cloth);
        }

        // POST: Clothes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cloth cloth = db.Clothes.Find(id);
            db.Clothes.Remove(cloth);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
