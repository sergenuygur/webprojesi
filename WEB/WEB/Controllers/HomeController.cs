﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WEB.Models;

namespace WEB.Controllers
{
    public class HomeController : Controller
    {
        private ElbiseStoreEntities db = new ElbiseStoreEntities();

        public ActionResult Index()
        {
            if (Session["user"] == null)
                return RedirectToAction("Login");

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }


        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Login(User user)
        {
            if (ModelState.IsValid)
            {
                var userV = db.Users.Where(u => u.Username.Contains(user.Username) && u.Password.Contains(user.Password)).FirstOrDefault();

                if (userV != null)
                {
                    Session["user"] = userV.Username;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(user);
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();

            base.Dispose(disposing);
        }
    }
}