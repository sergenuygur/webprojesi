﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WEB.Models;
using WEB.ViewModels;

namespace WEB.Controllers
{
    public class HomePageController : Controller
    {
        private ElbiseStoreEntities db = new ElbiseStoreEntities();

        // GET: HomePage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            Session["user"] = db.Users.Where(u => u.Username.Contains(user.Username)).FirstOrDefault().ID;

            return View(user);
        }

        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(User user)
        {
            var userV = db.Users.Where(u => u.Username.Contains(user.Username) && u.Password.Contains(user.Password)).FirstOrDefault();

            if (userV != null)
            {
                Session["ID"] = userV.ID;
                Session["user"] = userV.Username;
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "Invalid username or password.");
            }

            return View(user);
        }

        public ActionResult SignOut()
        {
            Session.Clear();

            return RedirectToAction("Index");
        }

        public ActionResult Profilim()
        {
            var user = db.Users.Find(Convert.ToInt32(Session["ID"].ToString()));

            if (user == null)
                return RedirectToAction("SignIn");

            return View(user);
        }

        public ActionResult About()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Urunler(string modelName = null, string color = null, Double size = -1, Double downPrice = -1, Double upPrice = -1)
        {
            var clothes = from c in db.Clothes
                          select c;

            if (!String.IsNullOrEmpty(modelName))
                clothes = clothes.Where(cloth => cloth.ModelName.Contains(modelName));

            if (!String.IsNullOrEmpty(color))
                clothes = clothes.Where(cloth => cloth.Color.Contains(color));

            if (size != -1)
                clothes = clothes.Where(cloth => cloth.Size == size);

            if (downPrice != -1)
                clothes = clothes.Where(cloth => cloth.Price >= downPrice);

            if (upPrice != -1)
                clothes = clothes.Where(cloth => cloth.Size <= upPrice);

            return View(clothes.ToList());
        }

        public ActionResult SepeteEkle(int clothID)
        {
            if (Session["user"] == null)
                return RedirectToAction("SignIn");

            OrderDetail orderDetail = new OrderDetail();

            var cloth = db.Clothes.Where(c => c.ID == clothID).FirstOrDefault();

            orderDetail.ClothID = clothID;
            orderDetail.Confirm = false;
            orderDetail.UserID = Convert.ToInt32(Session["ID"].ToString());

            db.OrderDetails.Add(orderDetail);
            db.SaveChanges();

            return RedirectToAction("Urunler", "HomePage");
        }

        public ActionResult SatinAl()
        {
            if (Session["user"] == null)
                return RedirectToAction("SignIn");

            var model = new SatinAlModels();

            model.siparis = new OrderDetail();

            model.sepet = db.OrderDetails.Include(o => o.cloth).Include(o => o.user);

            return View(model);
        }

        [HttpPost]
        public ActionResult SatinAl(SatinAlModels model)
        {
            model.sepet = db.OrderDetails.Where(o => o.UserID == Convert.ToInt32(Session["ID"].ToString()));

            foreach (var item in model.sepet)
            {
                item.Adress = model.siparis.Adress;
                item.PhoneNumber = model.siparis.PhoneNumber;
                item.Confirm = true;

                db.Entry(item).State = EntityState.Modified;
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact Page";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
