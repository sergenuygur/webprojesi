﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB.Models
{
    public class Cloth
    {
        public int ID { get; set; }
        public String ModelName { get; set; }
        public String Color { get; set; }
        public Double Size { get; set; }
        public Double Price { get; set; }
        public String ImageURL { get; set; }
    }
}