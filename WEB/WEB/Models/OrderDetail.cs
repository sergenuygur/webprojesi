﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WEB.Models
{
    public class OrderDetail
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int ClothID { get; set; }
        public decimal UnitPrice { get; set; }
        [DataType(DataType.PhoneNumber)]
        public String PhoneNumber { get; set; }
        public String Adress { get; set; }
        public bool Confirm { get; set; }
        public virtual Cloth cloth { get; set; }

        public virtual User user { get; set; }
    }
}
