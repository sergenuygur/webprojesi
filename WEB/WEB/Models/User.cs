﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WEB.Models
{
    public class User
    {
        public int ID { get; set; }
        public String Username { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        [DataType(DataType.Password)]
        public String Password { get; set; }
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }
        public String Gender { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode=true,DataFormatString="{0:yyyy-MM-dd}")]
        public DateTime BirthDay { get; set; }



    }
}