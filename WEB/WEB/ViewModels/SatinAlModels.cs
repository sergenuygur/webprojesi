﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WEB.Models;

namespace WEB.ViewModels
{
    public class SatinAlModels
    {
        public OrderDetail siparis { get; set; }
        public IEnumerable<OrderDetail> sepet { get; set; }
    }
}